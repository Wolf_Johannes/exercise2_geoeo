import pyproj
import shapely.ops as ops
from shapely.geometry.polygon import Polygon
from functools import partial

#bounding box polygon
geom = Polygon([(-78.041044, -58.155330), (-78.041044, -35.594970), (-58.155330, -35.594970), (-58.392606, -58.155330), (-78.041044, -58.155330)])

#area calculation in EPSG 4326
geom_area = ops.transform(
    partial(
        pyproj.transform,
        pyproj.Proj(init='EPSG:4326'),
        pyproj.Proj(
            proj='aea',
            lat_1=geom.bounds[1],
            lat_2=geom.bounds[3]
        )
    ),
    geom)

# Print the area in km^2
area=geom_area.area
print(area/(10**6), 'km^2')

#mass change in gigatons per year 
#0.134 from task f -->trendlinde equation
mass=-10**(-9)*area*0.1342*10**(-6)
print('Masschange',mass,'Gt/year')