import functions
import task_b
import task_c
import task_d
import numpy as np
import os
import csv
import pandas as pd

#importing constants from task c and b
lamda = task_c.lamda
theta = task_c.theta
M = task_c.M
R = task_c.R
rho = task_c.rho
k = task_c.k
coefficient_files = task_b.coefficient_files
cnm_list, snm_list = task_b.run_all()
bb_grid = task_c.bb_grid

#initializing an empty list for later ewh values
mean_ewh_list = []

#for each file (month):
for i in range(len(cnm_list)):
    #applying 400km filters from task d
    cnm, snm = task_d.apply_filter_to_coefficients(cnm_list[i], snm_list[i], 400000)
    #calculating EWH with EWH_fast function
    ewh = functions.calc_EWH_fast(lamda,theta,cnm,snm,M,R,rho,k)
    #initializing list for mean values of each months
    mean_ewh_curr_month = []
    #calculating and appending  ewh values from different positions into months list
    for e in range(len(bb_grid)):
        mean_ewh = ewh[e] * bb_grid[:,2][e]
        mean_ewh_curr_month.append(mean_ewh)
    #calculating the mean value of ewh per month and appending to ewh list
    mean_ewh_list.append(sum(mean_ewh_curr_month)/len(mean_ewh_curr_month))
    print(str(i) + " / 149 EWH weighted")

#generating month + year from file names
months = []
for i in coefficient_files:
    months.append(i[len(i)-11:-4])

#generating a pandas data frame to write the values into a csv file
mean_ewh_list = pd.DataFrame({"avg_ewh": mean_ewh_list})
mean_ewh_list.insert(1, "months",  months)
mean_ewh_list.to_csv("area_wg_mean_ewh.csv", sep=",")
