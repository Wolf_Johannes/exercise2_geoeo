import functions
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

#ussage of CSV file from task e)
mean_ewh_list = pd.read_csv("area_wg_mean_ewh.csv", sep=",")
mean_ewh_list = mean_ewh_list.iloc[:,1]
mean_ewh_list = mean_ewh_list.values.tolist()


#interpolate the missiing grace month 
#with the given function interp_missing_months
#from functions.py
dec_yr, values_interp=functions.interp_missing_months(mean_ewh_list)



#plot without trendline (remove the 3 apostrofs )
'''
plt.scatter(dec_yr, values_interp)
plt.xlabel("Year")
plt.ylabel("EWH")
plt.savefig('Task_f_1.png')
plt.show()
'''

#print(type(dec_yr),type(values_interp))

#plot without trendline saved as plot_task_f.png
#trendline equation is printed out
fig, ax = plt.subplots(1, 1, figsize=(9, 7), sharex=True, sharey=True)

z = np.polyfit(dec_yr,values_interp, 1)
p = np.poly1d(z)
print('trendline equation',p)
ax.plot(dec_yr,p(dec_yr),"r--")
ax.plot(dec_yr, values_interp)
plt.xlabel("Year")
plt.ylabel("EWH in m")
ax.set_title('Mean EWH change 2003-01 to 2016-12 ', fontsize=14)
plt.savefig('plot_task_f.png')
plt.show()
