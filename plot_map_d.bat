@echo off
gmt gmtset FORMAT_GEO_MAP ddd

REM --- MODIFY VARIABLES BELOW FOR YOUR REGION/FILES!! ----
REM --- furthermore, you have to adjust the spacing of the grid lines
REM --- and label spacing of the colorbar

REM --- filename of ascii file (without ending) that you want to plot
set file=data_file_d
REM --- grid resolution in geographical degrees
set grid_res=0.5
REM --- filename of polygon file that outlines your averaging region
set file_poly=data/area_of_interest/area_of_interest.txt
REM --- map projection (second line is example for Greenland)
set map_proj=M-120/18c
REM set map_proj=B-45/75/60/75/18c
REM --- region
set region=-78.041044/-58.392606/-58.155330/-35.594970
REM --- color palette
set color_palette=haxby
REM --- map title
set title=April 2008 (400km)

REM =============================================================
REM --- other variables ----
set ascii_file=%file%.txt
set grid_file=%file%.grd


gmt begin %file% png

REM --- create grid from given ascii file ---
gmt xyz2grd %ascii_file% -R%region% -r -I%grid_res% -G%grid_file% -V

REM gmt grdmath %grid_file% 0.000001 DIV = %grid_file%

REM --- create map ---
echo Map is created:

REM --- create color palette table ---
REM gmt makecpt -C%color_palette% -T-42/-28/0.001 -Z
gmt grd2cpt %grid_file% -C%color_palette% -Sh -Z 

REM --- plot grid ---
gmt grdimage -J%map_proj% -R%region% %grid_file% -Q
gmt psxy %file_poly% -W3,red
gmt coast -Bxa5g5 -Bya5g5 -BWESN+t"%title%" -W1p,80/80/80 -Di -V 

REM --- plot color scale ---
gmt colorbar -Dx0c/-2c+w17c/0.35c+h -B0.05+l"EWH [m]" -V 

gmt end show
@echo on