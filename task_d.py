import numpy as np
import matplotlib.pyplot as plt
import read_one_month as rom
import functions
from math import pi, cos, log, exp

R = 6378137
M = 5972000000000000000000000
rho = 1000

def gaussian_coefficients(radius, max_degree):
    """Calculation of gaussian filter coefficients for given maximum degree and filter radius
    
    Args:
    radius as int: filter radius in meter
    max_degree as int: maximum degree
    
    Returns:
    gaussian coefficients as NumpyArray"""

    #initial empty NumpyArray filled with zeros
    w = np.zeros(max_degree+1)
    #factor b for the calculation of the gaussian filter coefficients
    b = log(2)/(1-cos(radius/R))
    
    for n in range(w.size):
        if n > 1:
            w[n]=-(2*n-1)/b*w[n-1]+w[n-2]
            if w[n] < 1e-7:
                break
        elif n == 0:
            w[n]=1
        elif n == 1:
            w[n]=(1+exp(-2*b))/(1-exp(-2*b))-1/b

    return w

def apply_filter_to_coefficients(cnm, snm, filter_radius):
    """Applies the weights resulting gaussian filter method (with specified radius) to the coefficients cnm and snm.
    Warning: cnm and snm need to have the same dimension.
    
    Args:
    cnm as NumpyArray: cnm coefficients with degree n and order m
    snm as NumpyArray: snm coefficients with degree n and order m
    filter_radius as int: filter radius in meter

    Returns:
    cnm as NumpyArray: updated/filtered cnm coefficients
    snm as NumpyArray: updated/filtered snm coefficients"""
    
    #Copy cnm and snm to keep the original values
    cnm = np.copy(cnm)
    snm = np.copy(snm)
    #Calculate the gaussian filter coefficients
    w = gaussian_coefficients(filter_radius,96)

    #Apply the filter coefficients to cnm and snm
    degree, _ = cnm.shape
    for n in range(degree):
        cnm[n] = cnm[n] * w[n]
        snm[n] = snm[n] * w[n]

    return cnm, snm

def calc_degree_variances(cnm, snm):
    """Calculate the degree variances for given cnm and snm
    Warning: cnm and snm need to have the same dimension.

    Args:
    cnm as NumpyArray: cnm coefficients with degree n and order m
    snm as NumpyArray: snm coefficients with degree n and order m

    Returns:
    degree_variances as list: degree variances for each degree
    """
    degree, _ = cnm.shape
    degree_variances = []
    for n in range(1,degree):
        current_degree_value = 0
        for m in range(len(cnm[n])):
            current_degree_value += cnm[n][m]**2 + snm[n][m]**2
        #current_degree_value = sqrt(current_degree_value)
        degree_variances.append(current_degree_value)
    return degree_variances

def plot_degree_variances(cnm, snm):
    """Plot degree variances for different filter radii (200km, 300km, 400km and unfiltered data)

    Args:
    cnm as NumpyArray: cnm coefficients with degree n and order m
    snm as NumpyArray: snm coefficients with degree n and order m

    Returns:
    NaN    
    """
    # values for x-axis (degree)
    x = np.arange(1,97)

    # filtered coefficients
    cnm_filtered, snm_filtered = apply_filter_to_coefficients(cnm, snm, 200000)
    plt.plot(x, calc_degree_variances(cnm_filtered, snm_filtered), label="200km")

    cnm_filtered, snm_filtered = apply_filter_to_coefficients(cnm, snm, 300000)
    plt.plot(x, calc_degree_variances(cnm_filtered, snm_filtered), label="300km")

    cnm_filtered, snm_filtered = apply_filter_to_coefficients(cnm, snm, 400000)
    plt.plot(x, calc_degree_variances(cnm_filtered, snm_filtered), label="400km")

    # unfiltered coefficients
    plt.plot(x, calc_degree_variances(cnm, snm), label="unfiltered")

    # labels and title
    plt.yscale("log")
    plt.title("Degree variances")
    plt.xlabel("[degree]")
    plt.ylabel("[geoid heights]")
    plt.legend(loc="lower left")
    plt.show()

def plot_gaussian_coefficients_different_radii():
    """Plot gaussian filter coefficients for different filter radii (200km, 300km, 400km, 500km, 700km and 1000km)

    Args:
    NaN

    Returns:
    NaN    
    """
    x = np.arange(0,97)
    plt.plot(x, gaussian_coefficients(1000000,96), label="1000km")
    plt.plot(x, gaussian_coefficients(700000,96), label="700km")
    plt.plot(x, gaussian_coefficients(500000,96), label="500km")
    plt.plot(x, gaussian_coefficients(300000,96), label="300km")
    plt.plot(x, gaussian_coefficients(200000,96), label="200km")
    plt.title("Gaussian filter")
    plt.xlabel("[degree]")
    plt.ylabel("factor")
    plt.legend(loc="upper right")
    plt.show()

#load bounding box data
bb_data = np.loadtxt("data/area_of_interest/bounding_box.txt", delimiter=",")

#get grid out of data from existing getGrid function
bb_grid = functions.getGridfromPolygon(bb_data, 0.5)

#defining lamda value
lamda = bb_grid[:,0]

#calculating radians
lamda = lamda  * 2*pi/360

#defining theta (co-latitudes) and calculating radians
theta = (90 - bb_grid[:,1]) * 2*pi/360
#get cnm, snm matrizes
cnm, snm = rom.run_all()

# Plot degree variances have to be executed before filtering (please delete the "#" from following line to plot degree variances)
#plot_degree_variances(cnm, snm)

# Filtering with 400km radius
cnm, snm = apply_filter_to_coefficients(cnm, snm, 400000)

#getting load love numbers out of file
ll_data = np.loadtxt("data/loading/loadLoveNumbers_Gegout97.txt", delimiter=",")

#we just need first 97 values because of degree 0-96
k= ll_data[:97]

#calculation of ewh
ewh = functions.calc_EWH_fast(lamda,theta,cnm,snm,M,R,rho,k)

#write ewh values in txt file
with open('data_file_d.txt', 'w') as f:
    for n in range(len(lamda)):
        f.write(str(lamda[n]*180/pi) + "," + str(bb_grid[:,1][n]) + "," + str(ewh[n]))
        f.write("\n")

# Plot gaussian coefficients for different radii (please delete the "#" from following line to plot gaussian coefficients)
#plot_gaussian_coefficients_different_radii()
