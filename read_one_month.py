import functions
import numpy as np
import sys
from os import listdir
from os.path import isfile, join
#script to read just one file (used for task c)

np.set_printoptions(threshold=sys.maxsize)

#reading all files
path = "data/coefficients/"
path2 = "data/deg1/"

#declare filenames (in lists, so we don't need to change the function very much for task c)
coefficient_files = ["ITSG-Grace2018_n96_2008-04.gfc"]
deg1_files = ["deg1_2008-04.gfc"]

#function that reads file(s) from paths and identifies coefficients in them
def read_files(file_grace, file_degree1):
    #declare full file names
    file_grace = path + file_grace
    file_degree1 = path2 + file_degree1
    file_grace_static = "data/static/ITSG-Grace2018s.gfc"

    #read coefficient date with function from functions
    grace_cnm, grace_snm = functions.readData(file_grace, 21)
    degree1_cnm, degree1_snm = functions.readData(file_degree1, 13)

    #augmenting graces values for degree 1 with the deg1 data
    grace_cnm[1,1], grace_snm[1,1] = degree1_cnm[1,1], degree1_snm[1,1]
    grace_cnm[1,0], grace_snm[1,0] = degree1_cnm[1,0], degree1_snm[1,0]

    #loading grace_static field
    grace_static_cnm, grace_static_snm = functions.readData(file_grace_static, 13)

    #subtracting static grace from coefficients
    for i in range(len(grace_cnm)):
        for e in range(len(grace_cnm[i])):
            grace_cnm[i][e] = grace_cnm[i][e] - grace_static_cnm[i][e]
            grace_snm[i][e] = grace_snm[i][e] - grace_static_snm[i][e]

    return grace_cnm, grace_snm

#function to write them into lists (just relevant if there are multiple files)
def run_all():
    cnm_list = []
    snm_list = []
    for f in range(1):
        cnm, snm = read_files(coefficient_files[f], deg1_files[f])
        cnm_list.append(cnm)
        snm_list.append(snm)
    #just returns the 1st index because we just have one month in this case
    return cnm_list[0], snm_list[0]

#print(run_all())
