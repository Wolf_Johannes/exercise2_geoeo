import functions
import numpy as np
import sys
from os import listdir
from os.path import isfile, join
import csv
import pandas as pd

np.set_printoptions(threshold=sys.maxsize)

#reading all files
path = "data/coefficients/"
coefficient_files = [f for f in listdir(path) if isfile(join(path, f))]
coefficient_files.sort()

path2 = "data/deg1/"
deg1_files = [f for f in listdir(path2) if isfile(join(path2, f))]
deg1_files.sort()
#cutting off first elements of deg1 list (before 2003-01)
deg1_files = deg1_files[5:]

#function that reads file(s) from paths and identifies coefficients in them
def read_files(file_grace, file_degree1):
    #declare full file names
    file_grace = path + file_grace
    file_degree1 = path2 + file_degree1
    file_grace_static = "data/static/ITSG-Grace2018s.gfc"

    #read coefficient date with function from functions
    grace_cnm, grace_snm = functions.readData(file_grace, 21)
    degree1_cnm, degree1_snm = functions.readData(file_degree1, 13)

    #augmenting graces values for degree 1 with the deg1 data
    grace_cnm[1,1], grace_snm[1,1] = degree1_cnm[1,1], degree1_snm[1,1]
    grace_cnm[1,0], grace_snm[1,0] = degree1_cnm[1,0], degree1_snm[1,0]

    #loading grace_static field
    grace_static_cnm, grace_static_snm = functions.readData(file_grace_static, 13)

    #subtracting static grace from coefficients
    for i in range(len(grace_cnm)):
        for e in range(len(grace_cnm[i])):
            grace_cnm[i][e] = grace_cnm[i][e] - grace_static_cnm[i][e]
            grace_snm[i][e] = grace_snm[i][e] - grace_static_snm[i][e]

    return grace_cnm, grace_snm

#function to write them into lists (just relevant if there are multiple files)
def run_all():
    cnm_list = []
    snm_list = []
    for f in range(len(coefficient_files)):
        cnm, snm = read_files(coefficient_files[f], deg1_files[f])
        cnm_list.append(cnm)
        snm_list.append(snm)
        print(str(f) + " / 149")
    #returning a list of cnm/snm matrizes
    return cnm_list, snm_list
