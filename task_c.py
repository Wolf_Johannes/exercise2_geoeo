import read_one_month as rom
import functions
import numpy as np
from math import radians, pi, sin, cos
#script for task c

#load bounding box data
bb_data = np.loadtxt("data/area_of_interest/bounding_box.txt", delimiter=",")

#get grid out of data from existing getGrid function
bb_grid = functions.getGridfromPolygon(bb_data, 0.5)

#defining lamda value
lamda = bb_grid[:,0]
#calculating radians
lamda = lamda  * 2*pi/360
#defining theta (co-latitudes) and calculating radians
theta = (90 - bb_grid[:,1]) * 2*pi/360
#get cnm, snm matrizes
cnm, snm = rom.run_all()

#defining constants
M = 5972000000000000000000000
R = 6378137
rho = 1000

#getting load love numbers out of file
ll_data = np.loadtxt("data/loading/loadLoveNumbers_Gegout97.txt", delimiter=",")

#we just need first 97 values because of degree 0-96
k= ll_data[:97]

def calc_EWH(lamda, theta, cnm, snm, M, R, rho, k):
    #get maximum degree from shape of cnm
    max_degree = cnm.shape[0]
    #initialize NumpyArray with same shape as lamda for further calculated ewh values
    ewh = np.zeros(lamda.shape)
    #define first factor, which is independet from degree and order
    first_factor = M/(rho*4*pi*R**2)
    #iterate over co-latitudes and index (for further access to corresponding lamda value)
    for index, colatitude in enumerate(theta):
        #get pnm with given function
        pnm = functions.legendreFunctions(colatitude, max_degree)
        #setting outer sum to zero
        outer_sum = 0
        #iterate over all degrees
        for degree in range(max_degree):
            #setting inner sum to zero
            inner_sum = 0
            #calculation of the degree dependent factor
            degree_dependent_factor = (2*degree+1)/(1+k[degree])
            #iterate over all orders
            for order in range(max_degree):
                #calculation of the inner sum
                inner_sum += cnm[degree][order] * pnm[degree][order] * cos(order*lamda[index]) + snm[degree][order] * pnm[degree][order] * sin(order*lamda[index])
            #calculation of the outer sum
            outer_sum += inner_sum * degree_dependent_factor
        #apply the first factor to the outer sum
        ewh[index] = outer_sum * first_factor
    
    return(ewh)

# Old Version of our calc_EWH function (commented out)
"""
#function for calculating the EWH for all different positions
def calc_EWH(lamda,theta,cnm,snm,M,R,rho,k):
    # define maximum degree
    maxDegree = 96
    #defining an empty array for the ewh values
    ewh = np.array([0 for i in range(len(theta))], dtype="float")
    # calculate degree dependent factor
    #setting matrix for degrees (0-96)
    degree = np.array([i for i in range(0, maxDegree+1)], dtype="float")
    #calculating an radial factor for the current degree with respect to load love numbers
    radialFactor = (2*degree+1)/(1 + k)
    # calculate the first factor of the formula with Mass etc.
    first_factor = M/(rho*4*pi*(R**2))
    #setting matrix for the different orders (0-96)
    order = np.array([i for i in range(0, maxDegree+1)], dtype= "float")
    #defining a moving index for theta values
    current_index = 0
    # loop over all positions which we got from grid
    for i in range(len(theta)):
        #setting up a 2D array for Cnm and Snm (calculated from legendre and coefficients)
        Cnm = np.array([[0 for e in range(0, maxDegree+1)] for e in range(0, maxDegree+1)], dtype= "float")
        Snm = np.array([[0 for e in range(0, maxDegree+1)] for e in range(0, maxDegree+1)], dtype= "float")
        #declaring matrizes for the cos lamda and sin lamda values for current order
        cos_lamda = np.cos(np.multiply(lamda[i], order))
        sin_lamda = np.sin(np.multiply(lamda[i], order))
        #calculating the legendre functions for new position if theta changes
        #otherwise, the legendre function will have the same values and don't need to recalculated
        #so this will make the calculations a bit faster
        if (theta[i] != current_index):
            legendre_coefficients = functions.legendreFunctions(theta[i],maxDegree)
        #calculating the current Cnm and Snm by multiplying cos/sins with legendre coefficients
        for j in range(len(legendre_coefficients[1])):
            Cnm[:,j] = np.multiply(cos_lamda[j], legendre_coefficients[:,j])
            Snm[:,j] = np.multiply(sin_lamda[j], legendre_coefficients[:,j])
        #updating the index, so Legendre will get recalculated if changes
        current_index = theta[i]
        #calculating the sum for all orders (inner sum)
        all_order_values = np.sum(cnm*Cnm + snm*Snm,1)
        #calculating the sum of all degrees (outer sum)
        ewh[i] = first_factor*np.sum(radialFactor*all_order_values)
    return ewh"""

#save ewh in variable
ewh = calc_EWH(lamda,theta,cnm,snm,M,R,rho,k)

#(Test) Comparison with the result of the geven calc_EWH_fast function:
ewh_vergleich = functions.calc_EWH_fast(lamda,theta,cnm,snm,M,R,rho,k)
print(np.round(ewh,7)==np.round(ewh_vergleich,7))

#check if lenghts of different vectors are still the same
print(len(lamda))
print(len(theta))
print(len(ewh))

#writing the ewh values with coordinates into txt file
with open('data_file.txt', 'w') as f:
    for n in range(len(lamda)):
        f.write(str(lamda[n]*180/pi) + "," + str(bb_grid[:,1][n]) + "," + str(ewh[n]))
        f.write("\n")
