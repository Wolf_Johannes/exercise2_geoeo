#short script to generate plot for task e
import pandas as pd
import plotly.express as px

#reading ewh mean values for the area from generated csv file
df = pd.read_csv('area_wg_mean_ewh.csv')
df['months'] = pd.to_datetime(df['months'])

#initializing scatterplot with trendline
fig = px.scatter(df, x = 'months', y = 'avg_ewh', title='Feuerland Verlauf der EWH über die Jahre', trendline='ols')
fig.data[1].line.color = 'red'

#show plot
fig.show()
